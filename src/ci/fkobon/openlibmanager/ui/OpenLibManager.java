package ci.fkobon.openlibmanager.ui;

import ci.fkobon.openlibmanager.models.Clients;
import ci.fkobon.openlibmanager.views.ClientsViews;
import ci.fkobon.openlibmanager.views.OplmViews;
import java.util.Scanner;

/**
 * @author Francois KOBON
 * @version v0.1 
 */

public class OpenLibManager {

    static Scanner keyboard = new Scanner(System.in);
    static Clients client = new Clients();
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        char choix = ' ';
        
        boolean startApp = true;
                
        while(startApp){
            
            OplmViews.Accueil();
            choix = keyboard.next().charAt(0);
            keyboard.nextLine();
            System.out.println("-------------------------------------------");
            
            while(choix == '1'){
                ClientsViews.Accueil();
                choix = keyboard.next().charAt(0);
                keyboard.nextLine();
                
                while(choix == '1'){
                    ClientsViews.AjoutClient();
                    System.out.print("Ajouter un autre client (o/n) ? ");
                    System.out.println("-------------------------------------------");
                    char back = keyboard.next().charAt(0);
                    if(back == 'n') { break; }
                }
                
                while(choix == '2'){
                    ClientsViews.ListeClients();
                    System.out.print("Retour à Gestion Client (o/n) ? ");
                    char back = keyboard.next().charAt(0);
                    if(back == 'n') { break; }
                }
                
            }
            
            System.out.print("Retour au menu principal ? ");
            char back = keyboard.next().charAt(0);
            
            if(back == 'n') { startApp = false; System.out.println(startApp); }
        }
        
    }
    
}
