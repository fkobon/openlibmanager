package ci.fkobon.openlibmanager.models;

/**
 * @author Francois KOBON
 * @version v0.1 
 */

public class Bibliothecaire {
    
    private int id;
    private String nom, prenoms, login, motDePasse;

    /**
     * 
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @return the nom
     */
    public String getNom() {
        return nom;
    }
    
    /**
     * 
     * @return the prenoms
     */
    public String getPrenoms() {
        return prenoms;
    }

    /**
     * 
     * @return the login
     */
    public String getLogin() {
        return login;
    }

    /**
     * 
     * @return the motDePasse
     */
    public String getMotDePasse() {
        return motDePasse;
    }

    /**
     * 
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @param nom the nom to set
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

    /**
     * 
     * @param prenoms the prenoms to set
     */
    public void setPrenoms(String prenoms) {
        this.prenoms = prenoms;
    }

    /**
     * 
     * @param login the login to set
     */
    public void setLogin(String login) {
        this.login = login;
    }
 
    /**
     * 
     * @param motDePasse the motDePasse to set
     */
    public void setMotDePasse(String motDePasse) {
        this.motDePasse = motDePasse;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Bibliothecaire{" + "id=" + id + ", nom=" + nom + ", prenoms=" + prenoms + ", login=" + login + ", motDePasse=" + motDePasse + '}';
    }

    /**
     * 
     * Construire un objet Bibliothecaire avec tous ses paramètres
     * 
     * @param id l'Identification unique d'un bibliotheciare dans la base de donnée
     * @param nom l'nom du bibliothecaire
     * @param prenoms le(s) prénoms du bibliothécaire
     * @param login Login servant à se connecter au logiciel
     * @param motDePasse Mot de passe servant à se connecter au logiciel
     */
    public Bibliothecaire(int id, String nom, String prenoms, String login, String motDePasse) {
        this.id = id;
        this.nom = nom;
        this.prenoms = prenoms;
        this.login = login;
        this.motDePasse = motDePasse;
    }

    /**
     * Constructeur par défaut d'un Objet Bibliothecaire
     */
    public Bibliothecaire(){}
    
}
