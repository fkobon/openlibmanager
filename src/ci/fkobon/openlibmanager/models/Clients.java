package ci.fkobon.openlibmanager.models;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * @author Francois KOBON
 * @version v0.1 
 */

public class Clients {
    
    private int id;
    private String nom, prenoms, matricule, dateInscription;

    /**
     * @return the id
     */
    public int getId() {
            return id;
    }

    /**
     * @return the nom
     */
    public String getNom() {
            return nom;
    }

    /**
     * @return the prenoms
     */
    public String getPrenoms() {
            return prenoms;
    }

    /**
     * @return the matricule
     */
    public String getMatricule() {
            return matricule;
    }

    /**
     * @return the dateInscription
     */
    public String getDateInscription() {
            return dateInscription;
    }

    /**
     * @param id the id to set
     */
    public void setId(int id) {
            this.id = id;
    }

    /**
     * @param nom the nom to set
     */
    public void setNom(String nom) {
            this.nom = nom;
    }

    /**
     * @param prenoms the prenoms to set
     */
    public void setPrenoms(String prenoms) {
            this.prenoms = prenoms;
    }

    /**
     * @param matricule the matricule to set
     */
    public void setMatricule(String matricule) {
            this.matricule = matricule;
    }

    /**
     * @param dateInscription the dateInscription to set
     */
    public void setDateInscription(String dateInscription) {
            this.dateInscription = dateInscription;
    }
    
    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
            return "Client ajouter avec succès. [id=" + id + ", nom=" + nom + ", prenoms=" + prenoms + ", matricule=" + matricule
                            + ", dateInscription=" + dateInscription + "]";
    }
    
    /**
     * 
     * Méthode de génération de matricule pour chaque Clients
     * 
     * @return the matricule
     */
    public String genererMAtricule(){
        String chars = "1234567890";
        String matricule = "";
        for(int x=0; x<4; x++){
            int i = (int)Math.floor(Math.random() * 10);
            matricule += chars.charAt(i);
        }
        return "CLI-" + matricule;
    }
    
    /**
     * 
     * Construire un objet Clients avec tous ses paramètres
     * 
     * @param id l'identifiance unique d'un client de la bibliothèque
     * @param nom le nom d'un client de la bibliothèque
     * @param prenoms le prenom d'un client de la bibliothèque
     * @param matricule le matricule d'un client de la bibliothèque
     * @param dateInscription la date d'inscription d'un client de la bibliothèque
     */
    public Clients(int id, String nom, String prenoms, String matricule, String dateInscription) {
            this.id = id;
            this.nom = nom;
            this.prenoms = prenoms;
            this.matricule = matricule;
            this.dateInscription = dateInscription;
    }
    
    /**
     * 
     * Construire un objet Clients avec son nom et son prénom
     * Génération automatique de son numéro matricule et la date d'inscription
     * 
     * @param nom Nom du Client à créer
     * @param prenoms Prénoms du Client à créer
     */
    public Clients(String nom, String prenoms){
        this.nom = nom;
        this.prenoms = prenoms;
        
        // Récuperation de la date courante du système
        Date dateSysteme = new Date();
        DateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        dateInscription = formatDate.format(dateSysteme);
        
        matricule = genererMAtricule();
    }
    
    
    /**
     * Constructeur par défault d'un objet Clients
     */
    public Clients(){
        
        // Récuperation de la date courante du système
        Date dateSysteme = new Date();
        DateFormat formatDate = new SimpleDateFormat("dd/MM/yyyy");
        dateInscription = formatDate.format(dateSysteme);
        
        matricule = genererMAtricule();
    
    }
    
}
