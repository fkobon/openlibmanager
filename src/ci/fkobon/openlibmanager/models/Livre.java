package ci.fkobon.openlibmanager.models;

/**
 * @author Francois KOBON
 * @version v0.1 
 */

public class Livre {
    
    private int id;
    private double prix;
    private String titre, auteur;

    /**
     * 
     * @return the id
     */
    public int getId() {
        return id;
    }

    /**
     * 
     * @return the prix
     */
    public double getPrix() {
        return prix;
    }

    /**
     * 
     * @return the titre
     */
    public String getTitre() {
        return titre;
    }

    /**
     * 
     * @return the auteur
     */
    public String getAuteur() {
        return auteur;
    }

    /**
     * 
     * @param id the id to set
     */
    public void setId(int id) {
        this.id = id;
    }

    /**
     * 
     * @param prix the prix to set
     */
    public void setPrix(double prix) {
        this.prix = prix;
    }

    /**
     * 
     * @param titre the titre to set
     */
    public void setTitre(String titre) {
        this.titre = titre;
    }

    /**
     * 
     * @param auteur the auteur to set
     */
    public void setAuteur(String auteur) {
        this.auteur = auteur;
    }

    /* (non-Javadoc)
     * @see java.lang.Object#toString()
     */
    @Override
    public String toString() {
        return "Livre{" + "id=" + id + ", prix=" + prix + ", titre=" + titre + ", auteur=" + auteur + '}';
    }

    /**
     * 
     * Construire un objet Livre avec tous ses paramètres
     * 
     * @param id Clé unique d'un livre dans la base de donnée de la bibliothèque
     * @param prix le prix unitaire d'un livre
     * @param titre le titre d'un livre
     * @param auteur l'auteurs d'un livre
     */
    public Livre(int id, double prix, String titre, String auteur) {
        this.id = id;
        this.prix = prix;
        this.titre = titre;
        this.auteur = auteur;
    }

    
    /**
     * Constructeur par défaut d'un objet Livre
     */
    public Livre(){}
    
}
