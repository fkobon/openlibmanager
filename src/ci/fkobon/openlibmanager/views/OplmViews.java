package ci.fkobon.openlibmanager.views;

import ci.fkobon.openlibmanager.models.Clients;
import java.util.Scanner;

/**
 * @author Francois KOBON
 * @version v0.1 
 */

public class OplmViews {
    
    static Scanner keyboard = new Scanner(System.in);
    static Clients client = new Clients();
    
     /**
     * 
     * Vu d'accueil - Menu principal
     * 
     */
    public static void Accueil(){
        System.out.println("-------------------------------------------");
        System.out.println("|           OpenLib Manager v0.1          |");
        System.out.println("-------------------------------------------");
        System.out.println("|                                         |");
        System.out.println("|   1. Gestion des clients                |");
        System.out.println("|   2. Gestion des livres                 |");
        System.out.println("|   3. Un problème ? Aide                 |");
        System.out.println("|   4. Signaler un bug                    |");
        System.out.println("|   5. A propos                           |");
        System.out.println("|                                         |");
        System.out.println("-------------------------------------------");
        System.out.print("> Votre choix : ");   
    }
    
}
