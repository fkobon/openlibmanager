package ci.fkobon.openlibmanager.views;

import ci.fkobon.openlibmanager.models.Clients;
import java.util.Scanner;

/**
 * 
 * Classe des vues du modèle Clients
 * 
 * @author Francois KOBON
 * @version v0.1 
 */

public class ClientsViews {
    
    static Scanner keyboard = new Scanner(System.in);
    static Clients client = new Clients();
    
    
    
    /**
     * 
     * Vu d'accueil de l'espace Client
     * 
     */
    public static void Accueil(){
        System.out.println("-------------------------------------------");
        System.out.println("|           Gestion des Clients           |");
        System.out.println("-------------------------------------------");
        System.out.println("|                                         |");
        System.out.println("|   1. Ajouter un client                  |");
        System.out.println("|   2. Liste des Clients                  |");
        System.out.println("|   3. Gérer les abonnements              |");
        System.out.println("|   0. Retour au menu principal           |");
        System.out.println("|                                         |");
        System.out.println("-------------------------------------------");
    }
    
    /**
     * 
     * Vu d'ajout d'un nouveau Client
     * 
     */
    public static void AjoutClient(){
        System.out.println("-------------------------------------------");
        System.out.println("|           Ajouter un client             |");
        System.out.println("-------------------------------------------");
        System.out.print("> NOM : ");
        client.setNom(keyboard.nextLine());
        System.out.print("> PRENOMS : ");
        client.setPrenoms(keyboard.nextLine());
        System.out.println(client.toString());
        System.out.println("-------------------------------------------");
    }
    
    /**
     * 
     * Méthode listant tous les clients de la bibliothèque
     * 
     */
    public static void ListeClients(){
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println("|   N°  |   MATRICULES  |       NOMS        |       PRENOMS     |   DATE D'INSCRIPTION  |");
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println("|   1   | CLI-0006      | KOBON             | Francois-Xavier   | 25-04-2017            |");
        System.out.println("|   1   | CLI-0005      | LOULOU            | Beugré Alex       | 25-04-2017            |");
        System.out.println("|   1   | CLI-0004      | ROMBA             | Josué             | 24-04-2017            |");
        System.out.println("|   1   | CLI-0003      | MISSA             | Constant Pierre   | 24-04-2017            |");
        System.out.println("|   1   | CLI-0002      | BACHABI           | Barakiss          | 22-04-2017            |");
        System.out.println("|   1   | CLI-0001      | BAMBA             | Franck            | 01-04-2017            |");
        System.out.println("-----------------------------------------------------------------------------------------");
        System.out.println("| 1. Ajouter un client     /   2. Retour        / 0. Menu principal                     |");
        System.out.println("-----------------------------------------------------------------------------------------");
    }
}
